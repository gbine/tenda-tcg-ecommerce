# Tenda TCG - E-commerce de Estampas Colecionáveis

## Tecnologias utilizadas

- Vue
- Vuex
- Tailwind CSS (Utility First CSS Framework)
- Axios
- Json Server
- Vue Persist (Recarregando a página não perde o estado do vuex e consequentemente o carrinho não é perdido).
- Prettier (Lint de código)
- Webpack

## Fazendo o build

``` bash
# instalar dependencias
npm install

# instalar json server globalmente e npx
npm install -g npx json-server

# iniciar backend
npm run backend

# build para production com minificação
npm run build

# build para dev
npm run dev
```

## Melhorias

- [ ] Melhorar o feedback ao adicionar items no carrinho
- [ ] Adicionar opção para mudar quantidade de um item dentro do carrinho
