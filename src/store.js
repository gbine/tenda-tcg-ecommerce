import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersist from 'vuex-persist';
import { getAllPaginated as getAllProductsPaginated } from '@/services/products';

Vue.use(Vuex);

const state = {
    cartIsOpen: false,
    searchTerm: null,
    products: {},
    productsAdded: [],
};

const vuexPersist = new VuexPersist({
    key: 'tenda-tcg',
    storage: localStorage,
});

export const mutations = {
    addToCart(state, { productId, quantity }) {
        const product = state.productsAdded.find(item => item.productId === productId);

        if (product) {
            product.quantity += quantity;
            return;
        }
        state.productsAdded.push({
            productId,
            quantity,
        });
    },

    removeItem(state, productId) {
        const indexToRemove = state.productsAdded.findIndex(item => item.productId === productId);
        state.productsAdded.splice(indexToRemove, 1);
    },

    openCart(state) {
        state.cartIsOpen = true;
    },

    closeCart(state) {
        state.cartIsOpen = false;
    },

    clearCart(state) {
        state.productsAdded = [];
    },

    setItems(state, products) {
        state.products = products;
    },

    setSearchTerm(state, searchTerm) {
        state.searchTerm = searchTerm;
    },
};

export const getters = {
    order(state) {
        function createOrderItem(item) {
            return {
                quantity: item.quantity,
                product: state.products.find(p => p.id === item.productId),
            };
        }

        return state.productsAdded.map(item => createOrderItem(item));
    },

    totalQuantity(state) {
        return state.productsAdded.reduce((previous, current) => previous + current.quantity, 0);
    },

    subtotal(state) {
        return state.productsAdded.reduce((previous, current) => {
            const price = state.products.find(item => item.id === current.productId).price;
            const totalPrice = price * current.quantity;
            return previous + totalPrice;
        }, 0);
    },
};

export const actions = {
    async loadProducts({ commit }, payload = {}) {
        const _page = payload._page || 1;
        const _extraParams = payload._extraParams || {};
        const searchTerm = payload.searchTerm;

        const { data } = await getAllProductsPaginated(_page, _extraParams);
        commit('setItems', data);
        commit('setSearchTerm', searchTerm);
    },
};

export const store = new Vuex.Store({
    state,
    mutations,
    getters,
    actions,
    plugins: [vuexPersist.plugin],
});
