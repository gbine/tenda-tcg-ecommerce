import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App';
import { store } from './store';
import ProductList from './components/ProductList';
import ProductDetails from './components/ProductDetails';

Vue.config.productionTip = false;
Vue.use(VueRouter);

const routes = [
    { path: '/', component: ProductList, name: 'product-list' },
    { path: '/details/:productId', component: ProductDetails, name: 'product-details' },
];

const router = new VueRouter({
    mode: 'history',
    routes,
});

new Vue({
    el: '#app',
    template: '<App/>',
    components: { App },
    store,
    router,
});
